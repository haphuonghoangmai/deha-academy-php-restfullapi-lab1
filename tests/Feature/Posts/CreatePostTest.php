<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */

    public function user_can_create_post_if_data_is_valid()
    {
        $postCountBeforeCreate = Post::count();
        $dataCreate = [
            'name' => $this->faker->name,
            'body' => $this->faker->text
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $dataCreate['name'])
                    ->etc()
            )->etc()
        );

        $postCountAfterCreate = Post::count();
        $this->assertEquals($postCountBeforeCreate + 1, $postCountAfterCreate);
    }

    /** @test */

    public function user_can_not_create_post_if_name_is_null()
    {
        $dataCreate = [
            'name' => '',
            'body' => $this->faker->text
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) => $json->has(
                'errors',
                fn(AssertableJson $json) => $json->has('name')
                    ->etc()
            )->etc()
        );
    }

    /** @test */

    public function user_can_not_create_post_if_data_is_not_validate()
    {
        $dataCreate = [
            'name' => '',
            'body' => ''
        ];

        $response = $this->json('POST', route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn(AssertableJson $json) => $json->has(
                'errors',
                fn(AssertableJson $json) => $json->has('body')
                    ->has('name')
                    ->etc()
            )->etc()
        );
    }
}
